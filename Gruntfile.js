module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            build: {
                src: ['build']
            }
        },
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        src: [
                            // Capture all
                            '**',
                            // Exceptions below...
                            '!*.bck',
                            '!README.md',
                            '!cool-eform.html',
                            '!assets/**',
                            '!build/**',
                            // git
                            '!.git/**',
                            '!.gitignore',
                            // npm / grunt
                            '!Gruntfile.js',
                            '!package.json',
                            '!node_modules/**'
                        ],
                        dest: 'build/'
                    }
                ]
            }
        },
        makepot: {
            main: {
                options: {
                    mainFile: 'cool-eform.php',
                    exclude: ['build/.*'],
                    type: 'wp-plugin'
                }
            }
        },
        wp_readme_to_markdown: {
            main: {
                options: {
                    screenshot_url: '/assets/{screenshot}.png'
                },
                files: {
                    'README.md': 'readme.txt'
                }
            }
        },
        wp_deploy: {
            deploy: {
                options: {
                    plugin_slug: 'cool-eform',
                    build_dir: 'build',
                    assets_dir: 'assets'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-wp-i18n');
    grunt.loadNpmTasks('grunt-wp-readme-to-markdown');
    grunt.loadNpmTasks('grunt-wp-deploy');

    grunt.registerTask('readme', ['wp_readme_to_markdown:main']);
    grunt.registerTask('build', ['clean:build', 'copy:build']);
    grunt.registerTask('deploy', ['wp_deploy:deploy']);
}
